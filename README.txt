This theme provides the following page.html.twig variables

Custom Navigation
 - burger: Provides the burger for mobile navigation toggle as a variable.
 - main_menu: Provides the main navigarion wrapped in bootstrap classes to provide a mobile collapse as a variable.
 - account_menu: Provides the account menu as a variable.
 - footer_menu: Provides the footer menu as a variable.

 Global Text
 - copyright_text: Provides the copyright text set in the theme settings.
 - footer_text: Provides the footer text set in the theme settings.

 Social Media Profile Links
 - twitter_url: Provides the twitter url set in the theme settings.
 - facebook_url: Provides the facebook url set in the theme settings.
 - linkedin_url: Provides the linkedin url set in the theme settings.
