(function ($, Drupal) {
    Drupal.behaviors.mobileNav = {
        attach: function (context, settings) {
            // Uses the ".expanded" class which Drupal generates onto any menu item
            // that has children.
            // This is dependent on clean, unmodified Bootstrap nav markup.
            // The ".main-header" part of this selector will be site specific
            $(context).find(".header .expanded").each(function () {
                var $expanded = $(this); // Create reusable var for efficiency
                var $link = $expanded.find("> a"); // Select any anchor which is an immediate child
                var $sub = $expanded.find(".nav"); // Select any nave element
                // Prepend a replica of the first link to the front of the nav
                $sub.prepend("<li class='replicated-first-level'><a href='" + $($link[0]).attr("href") + "'>"
                    + $($link[0]).text() + "</a></li>");
                // Disable the click on mobile. You may need to modify this breakpoint
                // to match your site's mobile breakpoint
                $link.click(function (e) {
                    if (window.matchMedia("(max-width: 992px)").matches) {
                        e.preventDefault();
                        $expanded.toggleClass("mobile-active");
                    }
                });
            });
        }
    };

    Drupal.behaviors.mapCenter = {
        attach: function (context, settings) {

            if (typeof Drupal.geolocation == "undefined") {
                return false;
            }
            Drupal.geolocation.maps.forEach(function (map, index) {
                Drupal.geolocation.maps[index].settings.center = map.googleMap.getCenter();
            });


            Drupal.geolocation.addCallback(function () {
                google.maps.event.addDomListener(window, 'resize', function () {
                    Drupal.geolocation.maps.forEach(function (map) {
                        map.googleMap.setCenter(map.settings.center);
                    });
                });
            });
        }
    }

})(jQuery, Drupal);

