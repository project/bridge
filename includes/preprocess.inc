<?php
use Drupal\Core\Render\Element;
use Drupal\Core\Template\Attribute;

/**
 * @file
 * Functions relating to preprocessing of templates.
 */

/**
 * Implements hook_preprocess().
 */
function bridge_preprocess(&$variables, $hook, $info) {

  // @todo change this to a switch if another hook is needed.
  if ($hook == 'page_title') {
    // Add the page subtitle field if it exists to the page template.
    $node = Drupal::request()->attributes->get('node');
    if ($node && $node->hasField('field_page_subtitle')) {
      $variables['subtitle'] = $node->field_page_subtitle->view('full');
    }
  }


}


/**
 * Implements hook_preprocess_breadcrumb().
 */
function bridge_preprocess_breadcrumb(&$vars) {

  // Whether or not to show the current page as text at the end of the breadcrumb
  // is controlled by the show_current variable on the these settings page.
  if (theme_get_setting("show_current") == TRUE) {

    // Because the breadcrumb is different on each page lets make sure the cache
    // context includes url.
    $vars['#cache']['contexts'][] = 'url';

    // Access the node object for the current page.
    $node = \Drupal::request()->attributes->get('node');

    // If this page is a node.
    if (isset($node)) {

      // Build a breadcrumb item array.
      $current_crumb = array(
        'text' => $node->getTitle(),
      );

      // Append our new breadcrumb item to the existing breadcrumbs.
      $vars['breadcrumb'][] = $current_crumb;
    }

    $term =  \Drupal::request()->attributes->get('taxonomy_term');
    // If this page is a taxonomy term page.
    if (isset($term)) {
      // Build a breadcrumb item array.
      $current_crumb = array(
        'text' => $term->getName(),
      );

      // Append our new breadcrumb item to the existing breadcrumbs.
      $vars['breadcrumb'][] = $current_crumb;
    }

  }
}


/**
 * Implements hook_preprocess_region().
 */
function bridge_preprocess_region(&$vars) {

  $vars['switcher']= bridge_build_block('language_block:language_interface');

  // Set a variable based on the path.matcher.
  if (!isset($vars['is_front'])) {
    try {
      $vars['is_front'] = \Drupal::service('path.matcher')->isFrontPage();
    }
    catch (\Exception $e) {
      $vars['is_front'] = FALSE;
    }
  }

  // Ensure the cache varies correctly.
 // $vars['#cache']['contexts'][] = 'url.path.is_front';

  // Process each region separately.
  switch ($vars['region']) {

    case "header":

      // Add teh burger menu as a variable.
      $vars['burger'] = array(
        'burger' => array(
          '#theme' => 'burger'
        )
      );

      $vars['account_menu'] = bridge_build_menu('account','system_menu_block');
      $vars['account_menu']['menu']['#attributes'] = array('class' => array('nav', 'navbar-nav'));

      $vars['search_form']= bridge_build_block('search_form_block');
      break;

    case "footer":
      // Adds the footer navigation as a variable.
      $vars['footer_menu'] =  bridge_build_menu('footer','system_menu_block');
      $vars['footer_menu']['menu']['#attributes'] = array('class' => array('nav', 'navbar-nav'));

      break;


  }
}


/**
 * Implements hook_preprocess_page().
 */
function bridge_preprocess_page(&$vars) {

  // Is the current page an admin page?
  $vars['is_admin'] = \Drupal::service('router.admin_context')->isAdminRoute();

  // Loop through all the regions and force them to display if our theme setting
  // says they should render even when empty.
  $theme = \Drupal::theme()->getActiveTheme()->getName();
  foreach (system_region_list($theme) as $region_name => $region) {
    if (theme_get_setting('show_' .$region_name )) {
      if (empty($vars['page'][$region_name])) {
        $vars['page'][$region_name] = array(
          '#region' => $region_name,
          '#theme_wrappers' => array(
            'region'
          ),
          'empty' => array(
            '#markup' => ''
          )
        );
      }
    }
  }

  // Add the sitename as a variable.
  $vars['site_name'] = \Drupal::config('system.site')->get('name');

  // Add teh burger menu as a variable.
  $vars['burger'] = array(
    'burger' => array(
      '#theme' => 'burger'
    )
  );

  // Adds the main navigation as a variable.
  $vars['main_menu'] = bridge_build_menu('main','system_menu_block');
  $vars['main_menu']['#attributes']['class'] = array('collapse', 'navbar-collapse');


  // Adds the account navigation as a variable.
  $vars['account_menu'] = bridge_build_menu('account','system_menu_block');

  $vars['search_form']= bridge_build_block('search_form_block');

  // Global Text.
  $vars['copyright_text'] = theme_get_setting('copyright_text');
  $vars['footer_text'] = theme_get_setting('footer_text')['value']; // @todo what happened to check_markup?

  // Social Links.
  $vars['twitter_url'] = theme_get_setting('twitter_url');
  $vars['facebook_url'] = theme_get_setting('facebook_url');
  $vars['linkedin_url'] = theme_get_setting('linkedin_url');

}


/**
 * Implements hook_preprocess_file().
 */
function bridge_preprocess_file_link(&$vars) {

  $languageCode = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $vars['size'] = format_size($vars['file']->getSize(), $languageCode);

  $fileParts = pathinfo($vars['file']->getFileName());
  $vars['type'] = strtoupper($fileParts['extension']);


  $vars['icon_directory'] = drupal_get_path("theme", "bridge") . "/img/ico/file";
  $icon_class = file_icon_class($vars['file']->getMimeType());
  $vars['icon'] = array(
    '#theme' => 'image',
    '#uri' => $vars['icon_directory'] . "/" . $icon_class . ".png",
    '#alt' => "testing",
  );

}



function bridge_theme_suggestions_file_link_alter(array &$suggestions, array $variables) {


}

