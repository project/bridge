<?php
/**
 * @file
 * Functions that relate to altering forms, etc.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function bridge_form_system_theme_settings_alter(&$form, FormStateInterface &$form_state, $form_id = NULL) {
  if (isset($form_id)) {
    return;
  }

  $form['icons'] = array(
    '#type' => 'details',
    '#title' => t('Icons'),
    '#open' => TRUE,
  );

  $form['icons']['icon_directory'] = array(
    '#type' => 'textfield',
    '#title' => t("Icon Directory"),
    '#default_value' => theme_get_setting('icon_directory'),
  );


  $form['breadcrumb'] = array(
    '#type' => 'details',
    '#title' => t('Breadcrumb'),
    '#open' => TRUE,
  );

  $form['breadcrumb']['show_current'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show current page'),
    '#default_value' => theme_get_setting('show_current'),
    '#description' => t("Show current page in breadcrumb"),
  );

  $form['breadcrumb']['show_home'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show the home breadcrumb link'),
    '#default_value' => theme_get_setting('show_current'),
    '#description' => t("Show current page in breadcrumb"),
  );

  $form['global_text'] = array(
    '#type' => 'details',
    '#title' => 'Global Text',
    '#open' => TRUE,
  );

  $form['global_text']['copyright_text'] = array(
    '#title' => 'Copyright Text',
    '#type' => 'textfield',
    '#default_value' => theme_get_setting('copyright_text'),
  );
  $form['global_text']['footer_text'] = array(
    '#title' => 'Footer Text',
    '#type' => 'text_format',
    '#format' => 'basic_html',
    '#default_value' => theme_get_setting('footer_text')['value'],
  );

  $form['social_links'] = array(
    '#type' => 'details',
    '#title' => t('Social Links'),
    '#open' => TRUE,
    '#description' => 'Links to your social media profiles.',

  );
  $form['social_links']['twitter_url'] = array(
    '#type' => 'url',
    '#title' => t("Twitter Profile"),
    '#default_value' => theme_get_setting('twitter_url'),
  );
  $form['social_links']['facebook_url'] = array(
    '#type' => 'url',
    '#title' => t("Facebook Profile"),
    '#default_value' => theme_get_setting('facebook_url'),
  );
  $form['social_links']['linkedin_url'] = array(
    '#type' => 'url',
    '#title' => t("LinkedIn Profile"),
    '#default_value' => theme_get_setting('linkedin_url'),
  );
  $form['social_links']['youtube_url'] = array(
    '#type' => 'url',
    '#title' => t("Youtube Profile"),
    '#default_value' => theme_get_setting('youtube_url'),
  );
  $form['social_links']['instagram_url'] = array(
    '#type' => 'url',
    '#title' => t("Instagram Profile"),
    '#default_value' => theme_get_setting('instagram_url'),
  );
  $form['social_links']['newsletter_url'] = array(
    '#type' => 'url',
    '#title' => t("Newsletter URL"),
    '#default_value' => theme_get_setting('newsletter_url'),
  );

  $form['regions'] = array(
    '#type' => 'details',
    '#title' => t('Region Visibility'),
    '#open' => TRUE,
    '#description' => 'Which regions would you always like to render even if no blocks are assigned.',
  );

  $theme = \Drupal::theme()->getActiveTheme()->getName();
  foreach (system_region_list($theme) as $region_name => $region) {
    $form['regions']['show_' . $region_name] = array(
      '#type' => 'checkbox',
      '#title' => t('Always show ' . $region_name . ' region'),
      '#default_value' => theme_get_setting('show_' . $region_name),
      '#description' => t(""),
    );
  }

}

function bridge_form_views_exposed_form_alter(&$form, FormStateInterface &$form_state, $form_id = NULL) {
  $form['#attributes']['class'][] = 'views-exposed-form--horizontal';
  $form['actions']['submit']['#attributes']['class'][] = 'button';
  $form['actions']['submit']['#attributes']['class'][] = 'button--tertiary';
  $form['actions']['submit']['#attributes']['class'][] = 'button--view';

  $form['actions']['reset']['#attributes']['class'][] = "button";
  $form['actions']['reset']['#attributes']['class'][] = "button--link";


}

function bridge_form_user_login_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Turn autocomplete off for login form.
  $form['#attributes']['autocomplete'] = 'off';
}