<?php
use Drupal\Core\Render\Element;

/**
 * Build a renderable array from an existing menu block
 * @param $menu
 */
function bridge_build_menu($menu, $type, $container = TRUE) {
  $block_manager = \Drupal::service('plugin.manager.block');
  $account_menu_block = $block_manager->createInstance($type . ':' . $menu, []);
  $output = $account_menu_block->build();
  if ($container) {
    return array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array($menu . '-navigation'),
      ),
      'menu' => $output,
    );
  } else {
    return $output;
  }

}

/**
 * @param $block
 * @return array
 */
function bridge_build_block($block) {
  $block_manager = \Drupal::service('plugin.manager.block');
  $account_menu_block = $block_manager->createInstance($block, []);
  return $account_menu_block->build();
}


