<?php

/**
 * @file
 * Provides any markup rendered via custom theming.
 */


/**
 * Implements hook_theme().
 */
function bridge_theme($existing, $type, $theme, $path) {
  return array(
    'burger' => array(
      'variables' => array(),
      'template' => 'navigation/burger',
    ),
  );
}