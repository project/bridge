To create a sub theme of bridge

1. Extract starterkit/ to the custom themes/ directory
2. Rename all instances of "starterkit" to your module name
  starterkit/ -> <my_sub_theme>
  starterkit.starterkit-info.yml -> <my_sub_theme>.info.yml
  starterkit.libraries.yml -> <my_sub_theme>.info.yml
  starterkit.theme -> <my_sub_theme>.theme

   Inside starterkit.info.yml
   name: Starterkit -> name: <My Sub Theme>
   starterkit/global-styling -> <my_sub_theme>/global-styling
 3. Include the bootstrap-sass library in libraries/ and check the @import declared in <my_sub_theme>/scss/style.scss matches up with the bootstrap-sass directory.
 4. Run "compass watch" in <my_sub_theme>/ directory to create css directory and file.
 5. Install theme and set as default
 6. Boom